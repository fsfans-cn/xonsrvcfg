dirty solution for chat-to-sound because I can't write qc well

sound definition are lines of:

  <soundname> [aliases...]

if a chat message matches soundname or any of its aliases, the sound is played to all players
space by dashes in aliases, and all of these will work: foo-bar, foo_bar, foo bar, foobar
soundname should be a legal c variable name (limitation of how it does sound), and doesn't support unicode (limitation of compiler)
no need file extension for soundnames; all strings are compared in lowercase

you can also group sounds into subdirectories, by starting the path with a slash, with optional labels:

  /foo Foo

put sound files to ./sound/chat/[subdir] in data directory

after that, generate chattosound.qc by:

  go run genqc.go lib.go <defs...> >chattosound.qc

and optionally generate a quickmenu:

  go run genmenu.go lib.go <defs...> >quickmenu-chatsound.txt

if a sound definition has aliases, the first alias will be the primary name of the quickmenu entry, and players will say it, with - replaced by space

put generated chattosound.qc to smb modpack: ./mod/server/, then:

- in chat.qc, add the call ChatToSound(themsg) to somewhere of MUTATOR_HOOKFUNCTION(mod_chat, PreFormatMessage)
- in progs.inc, add #include "chattosound.qc" right before #include "chat.qc"

then compile the modpack, put compiled progs and soundpack to xonotic data directory, (re)start the game

show the quickmenu:

  quickmenu file "" quickmenu-chatsound.txt

you can also put the entry to your primary quickmenu:

  "Chat Sounds" "quickmenu; quickmenu file \"\" quickmenu-chatsound.txt"
