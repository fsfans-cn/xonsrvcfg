package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	os.Exit(cmain(len(os.Args), os.Args))
}

func read_tokens_to(paths []string, out chan []string) {
	for _, p := range paths {
		var def io.ReadCloser
		var err error
		if p == "-" {
			def = os.Stdin
		} else {
			def, err = os.Open(p)
		}
		if err != nil {
			fmt.Printf("cannot read %s: %s\n", p, err)
			continue
		}
		s := bufio.NewScanner(def)
		for s.Scan() {
			tokens := strings.Split(s.Text(), " ")
			if len(tokens[0]) == 0 || tokens[0][0] == '#' {
				continue
			}
			if tokens[0][0] == '/' {
				out <- tokens
				continue
			}
			out <- tokens
		}
		if def != os.Stdin {
			def.Close()
		}
	}
	close(out)
}
