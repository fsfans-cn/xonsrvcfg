package main

import (
	"fmt"
	"os"
	"strings"
)

func cmain(argc int, argv []string) int {
	if argc < 2 {
		fmt.Print(
			"usage: ./genqc [sound-definitions...]\n\n" +
				"see readme.txt about how to use\n",
		)
		return 1
	}
	out := os.Stdout
	const soundpath string = "sound/chat"
	subdir := "/"
	paths := argv[1:]
	out.WriteString("// generated: ./chattosoundgen " + strings.Join(paths, " ") + "\n\n")
	tokens := make(chan []string, 16)
	go read_tokens_to(paths, tokens)
	for t := range tokens {
		if t[0][0] == '/' {
			subdir = t[0]
			if subdir[len(subdir)-1] != '/' {
				subdir += "/"
			}
			continue
		}
		fmt.Fprintf(out, "SOUND(%s, \"%s\");\n", t[0], soundpath+subdir+t[0])
	}
	out.Write([]byte{'\n'})
	out.WriteString("void ChatToSound(string msg)\n{\n\tmsg = strtolower(msg);\n\tentity sound;\n\t")
	tokens = make(chan []string, 16)
	subdir = "/"
	go read_tokens_to(paths, tokens)
	for t := range tokens {
		if t[0][0] == '/' {
			continue
		}
		k := []string{}
		for _, s := range t {
			s = strings.ToLower(s)
			k = append(k, s)
			if strings.Contains(s, "-") {
				k = append(k, strings.ReplaceAll(s, "-", "_"), strings.ReplaceAll(s, "-", " "), strings.ReplaceAll(s, "-", ""))
			}
		}
		fmt.Fprintf(out, "if (msg == \"%s\")\n", strings.Join(k, "\" || msg == \""))
		fmt.Fprintf(out, "\t\tsound = SND_%s;\n\telse ", k[0])
	}
	fmt.Fprint(out, "\n\t\tsound = NULL;\n")
	fmt.Fprint(out, "\tif (sound != NULL)\n\t\tsound(NULL, CH_INFO, sound, VOL_BASEVOICE, ATTEN_NONE);\n")
	// fmt.Fprint(out, "\telse\n\t\t_sound(NULL, CH_INFO, msg, VOL_BASEVOICE, ATTEN_NONE);\n")
	fmt.Fprint(out, "}\n")
	// out.Close()
	return 0
}
