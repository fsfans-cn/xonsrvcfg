package main

import (
	"fmt"
	"os"
	"strings"
)

func cmain(argc int, argv []string) int {
	if argc < 2 {
		fmt.Print(
			"usage: ./genmenu [sound-definitions...]\n\n" +
				"see readme.txt about how to use\n",
		)
		return 1
	}
	out := os.Stdout
	paths := argv[1:]
	tokens := make(chan []string, 16)
	label := ""
	indent := ""
	go read_tokens_to(paths, tokens)
	for t := range tokens {
		if t[0][0] == '/' {
			if label != "" {
				fmt.Fprintf(out, "\"%s\"\n", label)
			}
			label = strings.Join(t[1:], " ")
			if label != "" {
				fmt.Fprintf(out, "\n\"%s\"\n", label)
				indent = "\t"
			} else {
				indent = ""
			}
			continue
		}
		if len(t) > 2 {
			t = t[:2]
		}
		if len(t) >= 2 {
			t[0], t[1] = t[1], t[0]
		}
		t[0] = strings.ReplaceAll(t[0], "-", " ")
		fmt.Fprintf(out, "%s\"%s\" \"say %s\"\n", indent, strings.Join(t, " "), t[0])
	}
	if label != "" {
		fmt.Fprintf(out, "\"%s\"\n", label)
	}
	// out.Close()
	return 0
}
