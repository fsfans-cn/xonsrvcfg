run this to generate bot files:
  go run main.go bots.txt.meta superbots.txt.meta

// meta syntax:
;model any|male|female|human|alien|<model-name>
;team none|red|blue|yellow|pink|0|1|2|3|4
;skill <integer>
;reset
// meta affects all following bot names until reset
