package main

import (
	"bufio"
	"crypto/sha256"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	os.Exit(cmain(len(os.Args), os.Args))
}

func cmain(argc int, argv []string) int {
	if argc < 2 {
		Println("usage:", argv[0], "[bot-meta-file.txt.meta...]")
		return 0
	}
	for _, path := range argv[1:] {
		RunStateFromFile(path)
	}
	return 0
}

func Println(args ...string) {
	os.Stdout.WriteString(args[0])
	for _, arg := range args[1:] {
		os.Stdout.Write([]byte{' '})
		os.Stdout.WriteString(arg)
	}
	os.Stdout.Write([]byte{'\n'})
}

const (
	TeamNone   = 0
	TeamRed    = 1
	TeamBlue   = 2
	TeamYellow = 3
	TeamPink   = 4

	TeamColorNone   = -1
	TeamColorRed    = 4
	TeamColorBlue   = 11
	TeamColorYellow = 12
	TeamColorPink   = 9
)

type State struct {
	randpool  []byte
	randindex int
	params    *BotParams
	skill     int
	team      string
	model     string
	skin      int
	r         io.Reader
	w         io.Writer
}

func (s *State) Cmd(args []string) {
	cmd := args[0]
	arg := ""
	if len(args) > 1 {
		arg = args[1]
	}
	switch cmd {
	case "reset":
		s.skill = 0
		s.team = ""
		s.model = ""
		s.skin = 0
	case "skill":
		s.skill, _ = strconv.Atoi(arg)
	case "team":
		s.team = arg
	case "model":
		s.model = arg
	case "skin":
		s.skin, _ = strconv.Atoi(arg)
	}
}

func (s *State) Hash(thing []byte) {
	hash := sha256.Sum256(thing)
	s.randpool = hash[:]
	s.randindex = 0
}

func (s *State) Rand() int {
	n := s.randpool[s.randindex]
	s.randindex++
	if s.randindex >= len(s.randpool) {
		s.randindex = 0
	}
	return int(n)
}

func (s *State) Run() {
	scanner := bufio.NewScanner(s.r)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			s.w.Write([]byte{'\n'})
			continue
		}
		if line[0:2] == "//" {
			s.w.Write([]byte(line + "\n"))
			continue
		}
		if line[0] == ';' {
			s.w.Write([]byte("//" + line + "\n"))
			s.Cmd(strings.Split(line[1:], " "))
			continue
		}
		bot := strings.Split(line, "\t")[0]
		s.Hash([]byte(bot))
		s.params.name = bot
		s.UseParams()
		s.w.Write([]byte(s.params.String() + "\n"))
	}
}

func (s *State) UseParams() {
	s.UseSkill()
	s.UseTeam()
	s.UseModel()
	s.UseSkin()
}

func (s *State) UseTeam() {
	r := s.Rand()
	a, b := r&0xf, (r&0xf0)>>4
	p := s.params
	switch s.team {
	case "", "0", "none":
		p.team = TeamNone
		p.shirt = a
		p.pants = b
	case "1", "red":
		p.team = TeamRed
		p.shirt = TeamColorRed
		p.pants = TeamColorRed
	case "2", "blue":
		p.team = TeamBlue
		p.shirt = TeamColorBlue
		p.pants = TeamColorBlue
	case "3", "yellow":
		p.team = TeamYellow
		p.shirt = TeamColorYellow
		p.pants = TeamColorYellow
	case "4", "pink":
		p.team = TeamPink
		p.shirt = TeamColorPink
		p.pants = TeamColorPink
	}
}

func (s *State) UseSkill() {
	r := s.Rand()
	f0 := s.skill * 10
	k1 := r & 0b1
	k2 := (r & 0b10) >> 1
	k3 := (r & 0b100) >> 2
	f1 := ((r >> 3) & 0b1) * 5
	f2 := (((r >> 4) & 0b11) - 1) * 5
	f3 := (((r >> 6) & 0b11) - 1) * 5
	p := s.params
	p.keyboard_use = f0 + f1*k1
	p.weapon_use = f0 + f1*k1
	p.mouse = f0 + f1*(k1^1)
	p.aiming = f0 + f1*(k1^1)
	p.aggressivity = f2 * k2
	p.range_ = f2 * k2
	p.fightthink = f2 * (k2 ^ 1)
	p.aithink = f2 * (k2 ^ 1)
	p.ping = f3 * k3
	p.moving = f3 * k3
	p.calmhand = f3 * (k3 ^ 1)
	p.dodging = f3 * (k3 ^ 1)
}

var model_pool [10]string = [10]string{
	"erebus", "ignis", "ignismasked",
	"nyx", "umbra", "pyria", "seraphina", "seraphinamasked",
	"gak", "gakmasked",
}

func (s *State) UseModel() {
	m := ""
	choose := func(pool []string) string {
		return pool[s.Rand()%len(pool)]
	}
	pool_any := model_pool[:]
	pool_male := model_pool[0:3]
	pool_female := model_pool[3:8]
	pool_human := model_pool[0:8]
	pool_alien := model_pool[8:10]
	switch s.model {
	case "", "any":
		m = choose(pool_any)
	case "male":
		m = choose(pool_male)
	case "female":
		m = choose(pool_female)
	case "human":
		m = choose(pool_human)
	case "alien":
		m = choose(pool_alien)
	default:
		m = s.model
	}
	s.params.model = m
}

func (s *State) UseSkin() {
	s.params.skin = s.skin
}

type BotParams struct {
	name         string
	model        string
	skin         int
	shirt        int
	pants        int
	team         int
	keyboard_use int
	moving       int
	dodging      int
	ping         int
	weapon_use   int
	aggressivity int
	range_       int
	aiming       int
	calmhand     int
	mouse        int
	fightthink   int
	aithink      int
}

func (p *BotParams) String() string {
	n := func(i int) string {
		return strconv.Itoa(i)
	}
	f := func(i int) string {
		if i%10 == 0 {
			return strconv.Itoa(i / 10)
		}
		sign := ""
		if i < 0 {
			sign = "-"
			i = -i
		}
		return sign + strconv.Itoa(i/10) + "." + strconv.Itoa(i%10)
	}
	return strings.Join([]string{
		p.name, p.model, n(p.skin), n(p.shirt), n(p.pants), n(p.team),
		f(p.keyboard_use), f(p.moving), f(p.dodging), f(p.ping), f(p.weapon_use),
		f(p.aggressivity), f(p.range_), f(p.aiming), f(p.calmhand), f(p.mouse),
		f(p.fightthink), f(p.aithink),
	}, "\t")
}

func NewState(r io.Reader, w io.Writer) *State {
	state := &State{
		make([]byte, 16), 0,
		&BotParams{},
		0, "", "", 0,
		r, w,
	}
	return state
}

func RunStateFromFile(path string) *State {
	target, ok := strings.CutSuffix(path, ".txt.meta")
	if !ok {
		Println(path+":", "not .txt.meta, processing anyway")
	}
	target = target + ".txt"
	r, err := os.OpenFile(path, os.O_RDONLY, 0)
	if err != nil {
		Println(path+":", "can't open for read:", err.Error())
		return nil
	}
	defer r.Close()
	w, err := os.OpenFile(target, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		Println(path+":", "can't open for write:", err.Error())
		return nil
	}
	defer w.Close()
	state := NewState(r, w)
	state.Run()
	return state
}
