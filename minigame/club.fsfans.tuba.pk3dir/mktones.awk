function make() {
    if (tones != "") {
        system(sprintf("./nokia2tuba.sh '%s' | awk -f clean.awk >'%s'", tones, cfgname))
        print _ title _ " " _ "exec " cfgname _ >>quickmenu
    }
    cfgname = ""
    title = ""
    tones = ""
}

BEGIN {
    _ = "\""
    quickmenu = "quickmenu-tuba.txt"
    print >quickmenu
    make()
}
/^$/ { make() }
END  {
    make()
    print "] quickmenu file " _ _ " " _ quickmenu _
}

{
    if(cfgname == "") {
        cfgname = "tuba-" $0 ".cfg"
        print cfgname
    } else if (title == "") {
        title = $0
    } else {
        tones = tones $0 " "
    }
}
