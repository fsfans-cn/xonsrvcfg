package main

import (
	"archive/zip"
	"crypto/sha256"
	"encoding/hex"
	"hash"
	"os"
	"path"
	"slices"
	"strconv"
	"strings"
	"time"
)

const bufsize int = 256 << 10

func main() {
	os.Exit(cmain(len(os.Args), os.Args))
}

func cmain(argc int, argv []string) int {
	if argc < 3 {
		os.Stdout.WriteString(
			"usage: ./mkpk3 <input-dir> <output-dir>\n" +
				"packages pk3dirs from input-dir to output-dir\n",
		)
		return 1
	}
	pathin := argv[1]
	pathout := argv[2]
	entries, err := os.ReadDir(pathin)
	if err != nil {
		os.Stdout.WriteString(err.Error())
		return 1
	}
	count := 0
	done := make(chan any, 16)
	for _, entry := range entries {
		path := path.Join(pathin, entry.Name())
		if !entry.IsDir() || !strings.HasSuffix(path, ".pk3dir") {
			continue
		}
		count++
		go func() {
			mkpk3(path, pathout)
			done <- nil
		}()
	}
	for count > 0 {
		<-done
		count--
	}
	return 0
}

func mkpk3(pk3dir, pathout string) {
	var v string
	verpath := path.Join(pk3dir, "version")
	if data, err := os.ReadFile(verpath); err == nil {
		v = strings.TrimSpace(string(data))
	} else {
		v = strconv.Itoa(time.Now().Local().Year()%100) + "01"
		// os.WriteFile(verpath, []byte(v), 0644)
	}
	name := strings.TrimSuffix(path.Base(pk3dir), ".pk3dir")
	filename := name
	target := path.Join(pathout, filename)
	f, err := os.OpenFile(target, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		os.Stdout.WriteString("cannot open " + pk3dir + " for write: " + err.Error())
		return
	}
	ar := zip.NewWriter(f)
	hs := sha256.New()
	// ar.AddFS(os.DirFS(pk3dir))
	buf := make([]byte, bufsize)
	add(pk3dir, "", ar, hs, buf)
	hssum := hs.Sum(nil)
	hsstr := hex.EncodeToString(hssum)
	filename += "_" + v + "+" + hsstr[0:4] + ".pk3"
	comment := filename + "\n" + hsstr + "\n"
	if !strings.HasSuffix(name, "-serveronly") {
		f, err := ar.Create(filename + ".serverpackage")
		if err == nil {
			f.Write([]byte(comment))
		}
	}
	ar.SetComment(comment)
	ar.Close()
	f.Close()
	os.Rename(target, path.Join(pathout, filename))
}

func add(dir, subdir string, ar *zip.Writer, hs hash.Hash, buf []byte) {
	fullpath := path.Join(dir, subdir)
	entries, err := os.ReadDir(fullpath)
	if err != nil {
		return
	}
	exclude := []string{"version", "include"}
	var include []string = nil
	if incdata, err := os.ReadFile(path.Join(fullpath, "include")); err == nil {
		include = strings.Split(string(incdata), "\n")
	}
	for _, e := range entries {
		name := e.Name()
		if len(include) > 0 && !slices.Contains(include, name) {
			continue
		}
		if slices.Contains(exclude, name) {
			continue
		}
		p := path.Join(subdir, name)
		if e.IsDir() {
			add(dir, p, ar, hs, buf)
		} else {
			d := path.Join(dir, p)
			f, err := os.Open(d)
			if err != nil {
				continue
			}
			w, err := ar.Create(strings.ReplaceAll(p, "\\", "/"))
			if err != nil {
				f.Close()
				continue
			}
			for {
				n, err := f.Read(buf)
				if err != nil {
					break
				}
				hs.Write(buf[:n])
				_, err = w.Write(buf[:n])
				if err != nil {
					break
				}
			}
			f.Close()
		}
	}
}
