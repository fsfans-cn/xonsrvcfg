#!/bin/sh
# no spaces in any directories involved, please
# target can be a list of directories, separate by space
if [ -z "$target" ]; then
    target="$HOME/.xonotic/data"
fi
if [ -z "$source" ]; then
    source=..
fi
if [ -z "$build" ]; then
    build=../build
fi

exe=mkpk3
go build -o $exe

for p in $(ls $build); do
    for t in $target; do
        rm -v "$t/$p"
    done
    rm $build/$p
done

mkdir -p $build
./$exe $source $build

for t in $target; do
    for p in $(ls $build); do
        echo ">> $t/$p"
        ln -s "`pwd`/$build/$p" "$t/$p"
    done
done
