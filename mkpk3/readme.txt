usage: go run main.go <input-dir> <output-dir>

packages pk3dirs from input-dir to output-dir
given pk3dir named <name>.pk3dir, pk3 name are in the format <name>_<version>+<hash>.pk3

if there's a file 'include', only files/folders listed in 'include' will be packaged
version is taken from the file 'version', default to calver YY01 if not exist
the files 'include' and 'version' are always excluded from package

hash is the first 2 bytes in hex of sha256 of all wrote data
if pk3dir name ends with -serveronly, the resulting pk3 will not contain a .serverpackage file, thus won't be given to clients
