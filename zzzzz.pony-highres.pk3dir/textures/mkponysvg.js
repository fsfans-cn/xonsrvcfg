
document.getElementById('svginput').addEventListener('change', function (event) {
    /** @type {File | undefined} */
    const file = event.currentTarget.files[0];
    if (file === undefined) return;
    const container = document.getElementById('frames');
    const frame = document.createElement('iframe');
    container.appendChild(frame);
    file.arrayBuffer().then(buffer => {
        frame.src = URL.createObjectURL(new Blob([buffer]));
        frame.addEventListener('load', function () {
            process(frame.contentDocument);
            frame.remove();
        }, { once: true });
    });
});

document.getElementById('frame').addEventListener('load', function (event) {
    /** @type {HTMLIFrameElement} */
    const frame = event.currentTarget;
    const svgdoc = frame.contentDocument;
    if (svgdoc === null)
        frame.remove();
    else
        process(svgdoc);
});

/** @param {Document} svgdoc */
function inkscape_label_to_id(svgdoc) {
    svgdoc.querySelectorAll('use').forEach(function (element) {
        const prototype = svgdoc.querySelector(element.getAttribute('xlink:href'));
        if (prototype !== null)
            element.setAttribute('xlink:href', '#' + prototype.getAttribute('inkscape:label'));
    });
    svgdoc.querySelectorAll('*').forEach(function (element) {
        if (element.hasAttribute('inkscape:label'))
            element.id = element.getAttribute('inkscape:label');
    });
}

/** @this {Document} */
function doc_blob() {
    return new Blob(['<?xml version="1.0" encoding="UTF-8" standalone="no"?>\n\n', this.documentElement.outerHTML]);
}

/**
 * @this {Document}
 * @param {string} id
 * @param {string} fill
 * @param {string} stroke
 */
function setfillstroke(id, fill, stroke) {
    const element = this.getElementById(id);
    if (element === null)
        return;
    if (fill)
        element.style.fill = fill;
    if (stroke)
        element.style.stroke = stroke;
}

const overrides = {
    'gloss': {
        'skin-left': 'black',
        'skin-right': 'black',
        'sclera': '#2a2a2a,#2a2a2a',
        'eyelash': '#2a2a2a',
        'iris': '#2a2a2a,#2a2a2a',
        'pupil': '#2a2a2a',
        'highlight': '#2a2a2a',
        'nose-left': '#2a2a2a',
        'mouth': '#2a2a2a'
    },
    'glow': {
        'skin-left': '#1f1f1f',
        'skin-right': 'white',
        'sclera': '#1f1f1f,#1f1f1f',
        'eyelash': '#1f1f1f',
        'iris': '#868686,#1f1f1f',
        'pupil': '#1f1f1f',
        'highlight': '#1f1f1f',
        'nose-left': '#1f1f1f',
        'mouth': '#1f1f1f'
    },
    'reflect': {
        'skin-left': 'black',
        'skin-right': 'black',
        'sclera': '#1a1a1a,#1a1a1a',
        'eyelash': '#1a1a1a',
        'iris': '#1a1a1a,#1a1a1a',
        'pupil': '#1a1a1a',
        'highlight': '#1a1a1a',
        'nose-left': '#1a1a1a',
        'mouth': '#1a1a1a'
    },
    'shirt': {
        'skin-left': '#c5c5c5',
        'skin-right': 'black',
        'sclera': 'black,black',
        'eyelash': 'black',
        'iris': 'white,white',
        'pupil': 'black',
        'highlight': 'black',
        'nose-left': '#949494',
        'mouth': '#949494'
    }
};

/** @param {Document} svgdoc */
function process(svgdoc) {
    if (svgdoc.getElementsByTagName('svg').length === 0) return;
    /** @type {doc_blob} */
    const blob = doc_blob.bind(svgdoc);
    /** @type {setfillstroke} */
    const set = setfillstroke.bind(svgdoc);
    const get = function(id) {
        const element = svgdoc.getElementById(id);
        return element.style.fill + ',' + element.style.stroke;
    }

    inkscape_label_to_id(svgdoc);
    
    const backup = {};
    const results = {
        'pony.svg': blob()
    };

    Object.entries(overrides).forEach(pair => {
        const name = pair[0];
        const override = pair[1];
        /** @type {typeof override} */
        Object.entries(override).forEach(pair => {
            const id = pair[0];
            const values = pair[1].split(',');
            const fill = values[0];
            const stroke = values[1];
            backup[id] = get(id);
            set(id, fill, stroke);
        });
        results['pony_' + name + '.svg'] = blob();
        Object.entries(backup).forEach(pair => {
            const id = pair[0];
            const values = pair[1].split(',');
            const fill = values[0];
            const stroke = values[1];
            set(id, fill, stroke);
        })
    });

    const out = document.getElementById('links');
    out.innerHTML = '';

    Object.entries(results).forEach(pair => {
        const name = pair[0];
        const blob = pair[1];
        const a = document.createElement('a');
        a.innerText = name;
        a.download = name;
        a.href = URL.createObjectURL(blob);
        const br = document.createElement('br');
        out.appendChild(a);
        out.appendChild(br);
    });
}
